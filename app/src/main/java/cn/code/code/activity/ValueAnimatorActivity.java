package cn.code.code.activity;

import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import cn.code.code.R;
import cn.code.code.util.Logger;

public class ValueAnimatorActivity extends AppCompatActivity {


    private TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_value_animator);

        textView = findViewById(R.id.textView);

        ObjectAnimator rotation = ObjectAnimator.ofFloat(textView, "Rotation", 0, 360);
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(textView, "ScaleX", 1, 2, 1);
        ObjectAnimator translationX = ObjectAnimator.ofFloat(textView, "TranslationX", 0, 100, 0, -100, 0);

        final AnimatorSet animatorSet= new AnimatorSet();
        animatorSet.setDuration(3000);
        animatorSet.play(rotation).with(scaleX).before(translationX);

        findViewById(R.id.startTv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animatorSet.start();
            }
        });
    }

    public class Cat{
        //当前猫的状态
        private String status;
        //毛发的数量
        private int hair;

        public Cat(String status, int hair) {
            this.status = status;
            this.hair = hair;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getHair() {
            return hair;
        }

        public void setHair(int hair) {
            this.hair = hair;
        }
    }

    public class MyEvaluator implements TypeEvaluator<Cat> {

        @Override
        public Cat evaluate(float fraction, Cat startCat, Cat endCat) {
            return new Cat("长毛中", (int) (startCat.getHair()+(endCat.getHair()-startCat.getHair())*fraction));
        }
    }

    public class MyInterpolator implements Interpolator {

        @Override
        public float getInterpolation(float input) {
            if (input<0.5f){
                return input;
            }else{
                return 1-input;
            }
        }
    }

}
