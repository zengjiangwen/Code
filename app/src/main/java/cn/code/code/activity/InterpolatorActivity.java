package cn.code.code.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.interpolator.view.animation.FastOutLinearInInterpolator;

import cn.code.code.R;
import cn.code.code.wiget.CoordView;

public class InterpolatorActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final View greenBall = findViewById(R.id.greenBall);
        final View redBall = findViewById(R.id.redBall);
        final View blueBall = findViewById(R.id.blueBall);

        final CoordView coordView = findViewById(R.id.coordView);


        Button playBtn = findViewById(R.id.playBtn);


        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float maxFactor=3.0f,minFactor=0.6f;
                int y=((ViewGroup)redBall.getParent()).getHeight()-redBall.getHeight();
                int duration=2000;
                final Animation redBallAnimation = new TranslateAnimation(0,0, 0,y);
                redBallAnimation.setFillAfter(true);
                redBallAnimation.setDuration(duration);


                redBallAnimation.setInterpolator(new FastOutLinearInInterpolator());

//                final Animation greenBallAnimation = new TranslateAnimation(0,0, 0,y);
//                greenBallAnimation.setFillAfter(true);
//                greenBallAnimation.setDuration(duration);
//                greenBallAnimation.setInterpolator(new CycleInterpolator(maxFactor));
//
//                final Animation blueBallAnimation = new TranslateAnimation(0,0, 0,y);
//                blueBallAnimation.setFillAfter(true);
//                blueBallAnimation.setDuration(duration);
//                blueBallAnimation.setInterpolator(new CycleInterpolator(minFactor));
//
                redBall.startAnimation(redBallAnimation);
//                greenBall.startAnimation(greenBallAnimation);
//                blueBall.startAnimation(blueBallAnimation);

//                coordView.startLinearInterpolator(duration);
//                coordView.startAccelerateInterpolator(duration,maxFactor,minFactor);
//                coordView.startAccelerateDecelerateInterpolator(duration);
//                coordView.startAnticipateInterpolator(duration,maxFactor,minFactor);
//                coordView.startBounceInterpolator(duration);
//                coordView.startOvershootInterpolator(duration,maxFactor,minFactor);
//                coordView.startAnticipateOvershootInterpolator(duration,maxFactor,minFactor);
//                coordView.startLinearOutSlowInInterpolator(duration);
//                coordView.startFastOutSlowInInterpolator(duration);
                coordView.startFastOutLinearInInterpolator(duration);


            }
        });
    }
}
