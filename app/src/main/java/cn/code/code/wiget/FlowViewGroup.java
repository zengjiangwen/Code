package cn.code.code.wiget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class FlowViewGroup extends ViewGroup {

    private final List<List<View>> mAllChildViews;

    public FlowViewGroup(Context context) {
        this(context, null);
    }

    public FlowViewGroup(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FlowViewGroup(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mAllChildViews = new ArrayList<>();
    }

    //如果想获取到child的margin的画，就得这么干。
    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new MarginLayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int left = getPaddingLeft();
        int top = getPaddingTop();
        int lineNumber = mAllChildViews.size();

        for (int i = 0; i < lineNumber; i++) {
            List<View> lineViews = mAllChildViews.get(i);
            for (int j = 0; j < lineViews.size(); j++) {
                View child = lineViews.get(j);
                MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();
                int lc = left + lp.leftMargin;
                int tc = top + lp.topMargin;
                int rc = lc + child.getMeasuredWidth();
                int bc = tc + child.getMeasuredHeight();
                child.layout(lc, tc, rc, bc);

                left += child.getMeasuredWidth() + lp.rightMargin
                        + lp.leftMargin;
            }
            left = getPaddingLeft();
            View child=lineViews.get(0);
            MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();
            top += child.getMeasuredHeight()+lp.topMargin+lp.bottomMargin;
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);

        //如果宽度是wrap_cotent，直接运行异常，因为宽度必须指定才符合我们的流式标签布局要求
        if (widthMode == MeasureSpec.AT_MOST) {
            throw new RuntimeException("layout_width不能设置为wrap_content!");
        }
        //一下根据子View的摆放，求得FlowViewGroup的高
        int height = getPaddingTop() + getPaddingBottom();
        // 行宽
        int lineWidth = 0;
        // 行高
        int lineHeight = 0;
        int childCount = getChildCount();

        mAllChildViews.clear();
        List<View> lineViews = new ArrayList<>();

        for (int i = 0; i < childCount; i++) {
            View childView = getChildAt(i);
            measureChild(childView, widthMeasureSpec, heightMeasureSpec);

            MarginLayoutParams lp = (MarginLayoutParams) childView.getLayoutParams();

            int childWidth = childView.getMeasuredWidth() + lp.leftMargin + lp.rightMargin;
            int childHeight = childView.getMeasuredHeight() + lp.topMargin + lp.bottomMargin;

            // 换行
            if (childWidth + lineWidth > (widthSize - getPaddingRight() - getPaddingLeft())) {

                //行剩余没有用上的空间平均分给子View的padding
                int space = widthSize - lineWidth;
                int perPadding = space / (lineViews.size() * 2);
                for (View lineView : lineViews) {
                    int l = lineView.getPaddingLeft() + perPadding;
                    int r = lineView.getPaddingRight() + perPadding;
                    int t = lineView.getPaddingTop();
                    int b = lineView.getPaddingBottom();
                    lineView.setPadding(l, t, r, b);
                }

                height += lineHeight;
                lineWidth = childWidth;

                // 添加一行
                mAllChildViews.add(lineViews);

                lineViews = new ArrayList<View>();
                lineViews.add(childView);

            } else {  // 不换行
                //所有的child的高都是一样的
                lineHeight = childHeight;
                lineWidth += childWidth;
                lineViews.add(childView);
            }

            //添加最后一行
            if (i == childCount - 1) {
                height += lineHeight;
                mAllChildViews.add(lineViews);
            }
        }
        //重新测量一遍
        measureChildren(widthMeasureSpec,heightMeasureSpec);
        setMeasuredDimension(widthSize, height);
    }


    public void setLabs(String[] labs) {
        TextView textView;
        for (String lab : labs) {
            textView = new TextView(getContext());
            addView(textView);
            textView.setTextSize(14);
            textView.setTextColor(Color.WHITE);
            textView.setGravity(Gravity.CENTER);
            textView.setText(lab);
            textView.setBackgroundColor(Color.BLUE);
            textView.setPadding(10,10,10,10);
            MarginLayoutParams lp = (MarginLayoutParams) textView.getLayoutParams();
            lp.setMargins(10, 10, 10, 10);
        }
    }
}
