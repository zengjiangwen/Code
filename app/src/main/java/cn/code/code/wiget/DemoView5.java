package cn.code.code.wiget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.DiscretePathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SumPathEffect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import cn.code.code.R;
import cn.code.code.util.Logger;

public class DemoView5 extends View {
    private Paint mPaint;

    public DemoView5(Context context) {
        this(context, null);
    }

    public DemoView5(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DemoView5(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public DemoView5(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private Bitmap mBitmap;
    private boolean hasSetShader;

    //初始化画笔Paint
    private void init() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        //加载bitmap图片
        mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.girl);

    }


    @Override
    protected void onDraw(Canvas canvas) {

        if (!hasSetShader) {
            hasSetShader=true;
            //用canvas.drawBitmap的方式，把上面加载的bitmap缩放到控件尺寸大小
            Bitmap bitmapShader = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvasBg = new Canvas(bitmapShader);
            canvasBg.drawBitmap(mBitmap, null, new RectF(0, 0, getWidth(), getHeight()), mPaint);

            //设置Shader,模式随意，因为图片已经缩放到控件大小，没有多余的控件应用重复策略
            mPaint.setShader(new BitmapShader(bitmapShader, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        }
        if (mX > 0 && mY > 0){
            canvas.drawCircle(mX, mY, 100, mPaint);
            canvas.drawCircle(mX+150, mY, 100, mPaint);
        }


    }

    private float mX, mY;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mX = event.getX();
                mY = event.getY();
                return true;
            case MotionEvent.ACTION_MOVE:
                mX = event.getX();
                mY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                mX = 0;
                mY = 0;
                break;
        }
        invalidate();
        return super.onTouchEvent(event);
    }
}
