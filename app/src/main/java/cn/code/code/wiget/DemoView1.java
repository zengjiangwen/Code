package cn.code.code.wiget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import cn.code.code.util.Logger;

public class DemoView1 extends View {
    private Paint mPaint;

    public DemoView1(Context context) {
        this(context, null);
    }

    public DemoView1(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DemoView1(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public DemoView1(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    //初始化画笔Paint
    private void init() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    }

    @Override
    protected void onDraw(Canvas canvas) {


        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setStrokeWidth(1);
        mPaint.setColor(Color.YELLOW);
        mPaint.setTextSize(48);//设置文字大小为12像素
        String text = "abcdefghijklmnopqrst";
        float textWidth = mPaint.measureText(text);
        int x = 100, y = 100;
        Paint.FontMetrics fontMetrics = mPaint.getFontMetrics();
        float top = fontMetrics.top;
        float ascent = fontMetrics.ascent;
        float descent = fontMetrics.descent;
        float bottom = fontMetrics.bottom;
        mPaint.setColor(Color.YELLOW);
        canvas.drawLine(x, top + y, x + textWidth, top + y, mPaint);
        mPaint.setColor(Color.RED);
        canvas.drawLine(x, ascent + y, x + textWidth, ascent + y, mPaint);
        mPaint.setColor(Color.RED);
        canvas.drawLine(x, y, x + textWidth, y, mPaint);
        mPaint.setColor(Color.WHITE);
        canvas.drawLine(x, descent + y, x + textWidth, descent + y, mPaint);
        mPaint.setColor(Color.GREEN);
        canvas.drawLine(x, bottom + y, x + textWidth, bottom + y, mPaint);
        mPaint.setColor(Color.WHITE);

        mPaint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText(text, x, y, mPaint);
    }



}
