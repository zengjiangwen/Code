package cn.code.code.wiget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class DemoView6 extends View {
    private Paint mPaint;

    public DemoView6(Context context) {
        this(context, null);
    }

    public DemoView6(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DemoView6(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public DemoView6(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    //初始化画笔Paint
    private void init() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    }


    @Override
    protected void onDraw(Canvas canvas) {

        float perWidth = 100;//每个shader占用的大小
        float offset=perWidth/2f;//两个 shader 之间的偏移量
        float perSpace = (getWidth()-3*(perWidth*2-offset))/2f;// 隔开的空间
        float modeWidth=2*perWidth-offset;

        PorterDuff.Mode[][] modes=new PorterDuff.Mode[][]{
                new PorterDuff.Mode[]{PorterDuff.Mode.CLEAR,PorterDuff.Mode.ADD,PorterDuff.Mode.DARKEN},
                new PorterDuff.Mode[]{PorterDuff.Mode.DST,PorterDuff.Mode.DST_ATOP,PorterDuff.Mode.DST_IN},
                new PorterDuff.Mode[]{PorterDuff.Mode.DST_OUT,PorterDuff.Mode.DST_OVER, PorterDuff.Mode.LIGHTEN},
                new PorterDuff.Mode[]{PorterDuff.Mode.MULTIPLY,PorterDuff.Mode.OVERLAY,PorterDuff.Mode.SCREEN},
                new PorterDuff.Mode[]{PorterDuff.Mode.SRC,PorterDuff.Mode.SRC_ATOP,PorterDuff.Mode.SRC_IN},
                new PorterDuff.Mode[]{PorterDuff.Mode.SRC_OUT,PorterDuff.Mode.SRC_OVER,PorterDuff.Mode.XOR}
        };
        //创建Bitmap,并填充0xffff0000红色
        Bitmap bitmap = Bitmap.createBitmap((int)perWidth, (int)perWidth, Bitmap.Config.ARGB_8888);
        Canvas canvasA=new Canvas(bitmap);
        canvasA.drawColor(0xffff0000);

        //创建BitmapShader
        BitmapShader bitmapShader=new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        float left,top,right,bottom;
        for (int i = 0; i < modes.length; i++) {

            for (int j=0;j<modes[i].length;j++){

                left=(modeWidth+perSpace)*j;
                top=(modeWidth+40)*i;
                right=left+perWidth;
                bottom=top+perWidth;

                //创建LinearGradient
                LinearGradient linearGradient=new LinearGradient(left,top,right,top,0xffff0000,0xff00ff00, Shader.TileMode.CLAMP);
                //设置画笔着色器（填充器）
                mPaint.setShader(linearGradient);
                canvas.drawRect(left,top,right,bottom,mPaint);

                //设置画笔着色器（填充器）
                mPaint.setShader(bitmapShader);
                canvas.drawRect(left+offset,top+offset,right+offset,bottom+offset,mPaint);

                //组合两个Shader
                ComposeShader composeShader = new ComposeShader(bitmapShader, linearGradient,modes[i][j]);

                //设置画笔着色器（填充器）
                mPaint.setShader(composeShader);
                canvas.drawRect(left+offset,top+offset,right,bottom,mPaint);

                mPaint.setShader(null);
                mPaint.setColor(Color.WHITE);
                canvas.drawText(modes[i][j].name(),left,top+modeWidth+20,mPaint);

            }
        }
    }

}
