package cn.code.code.wiget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import cn.code.code.R;

public class MyGroupView extends ViewGroup {

    //当创建的方式是 new MyView(context)时调用
    public MyGroupView(Context context) {
        this(context, null);
    }

    //当创建的方式是 xml布局的方式时调用
    public MyGroupView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyGroupView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        measureChildren(widthMeasureSpec,heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        View child=getChildAt(0);
        child.layout(0,0,child.getMeasuredWidth(),child.getMeasuredHeight());
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return super.onInterceptTouchEvent(ev);
    }


    float mX, mY;
    float dx, dy;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //这里要使用getRawX，来获取相对于屏幕的坐标
                mX = event.getRawX();
                mY = event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                dx = event.getRawX() - mX;
                dy = event.getRawY() - mY;
                getChildAt(0).setTranslationX(getChildAt(0).getX() + dx);
                getChildAt(0).setTranslationY(getChildAt(0).getY() + dy);
                mX = event.getRawX();
                mY = event.getRawY();
                break;
            case MotionEvent.ACTION_UP:

                break;
        }
        return true;
    }
}
