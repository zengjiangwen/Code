package cn.code.code.wiget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import cn.code.code.util.Logger;

public class DemoView3 extends View {
    private Paint mPaint;

    public DemoView3(Context context) {
        this(context, null);
    }

    public DemoView3(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DemoView3(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public DemoView3(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        mPaint.setColor(Color.RED);//设置画笔颜色
        mPaint.setStyle(Paint.Style.FILL);//设置画笔描边

//        canvas.drawPosText("安卓天下第一",new float[]{23,32,43,21,54,76,90,100,45,156,49,123},mPaint);

        mPaint.setColor(Color.RED);//设置画笔颜色
        mPaint.setStyle(Paint.Style.STROKE);//设置画笔描边
        mPaint.setStrokeWidth(10);//设置线宽
        mPaint.setLetterSpacing(0.3f);//设置字间距

        Path path=new Path();//新建一个路径
        path.addCircle(400,400,100, Path.Direction.CW);//在路径上添加一个圆
        Matrix matrix=new Matrix();//新建一个Matrix矩阵，用来旋转路径
        matrix.setRotate(90,400,400);//路径旋转90度
        path.transform(matrix);
        canvas.drawPath(path,mPaint);

        String text="深圳市腾讯计算机技术有限公司";
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setTextSize(20);
        canvas.drawTextOnPath(text,path,130,30,mPaint);

        String text1="南山总部";
        Path path1=new Path();
        path1.addCircle(400,400,100, Path.Direction.CCW);
        canvas.drawTextOnPath(text1,path1,420,-20,mPaint);


        //画五角星

        double arcAngle=(72*Math.PI)/180;
        float pointX[]=new float[5],pointY[]=new float[5];

        //以五角星中心点为（0，0），定义五角星右下角一个角点（40，40），通过这个点来计算五角星的其它点坐标
        pointX[0]=40;
        pointY[0]=40;
        for(int i=1;i<5;i++)
        {
            pointX[i]=(int)(pointX[i-1]*Math.cos(arcAngle)-pointY[i-1]*Math.sin(arcAngle));
            pointY[i]=(int)(pointY[i-1]*Math.cos(arcAngle)+pointX[i-1]*Math.sin(arcAngle));
        }


        Path star=new Path();
        star.moveTo(pointX[0],pointY[0]);
        for (int i=0;i<5;i+=2){
            star.lineTo(pointX[i],pointY[i]);
        }
        for (int i=1;i<5;i+=2){
            star.lineTo(pointX[i],pointY[i]);
        }
        //讲五角星的中心点移动到我们圆心的位置
        canvas.translate(400,400);
        canvas.drawPath(star,mPaint);

    }



}
